# želva - risanje v Python-u
# turtle - draw in Python

import turtle

print('Pozdravljen zelvak')

# Ustvarimo okno
okno = turtle.Screen()
# Ustvarimo zelvico
leonardo = turtle.Turtle()

# Izri�emo kvadrat s premikanjem naprej in zavijanjem v levo
leonardo.forward(100)
leonardo.right(90)
leonardo.forward(100)
leonardo.right(90)
leonardo.forward(100)
leonardo.right(90)
leonardo.forward(100)

turtle.done()

print('Zakljucujem...')