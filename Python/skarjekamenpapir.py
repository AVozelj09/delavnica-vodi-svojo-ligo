# Kamen, Škarje, Papir igra v Python-u
# Rock, Paper, Scissors in Python

from random import randint
 
#create a list of play options
t = ["Kamen", "Papir", "Škarje"]
 
#assign a random play to the computer
computer = t[randint(0,2)]
 
#set player to False
player = False
 
while player == False:
#set player to True
    player = input("Kamen, Škarje, Papir?")
    if player == computer:
        print("Izenačeno!")
    elif player == "Kamen":
        if computer == "Papir":
            print("Izgubil si!", computer, "prekrije", player)
        else:
            print("Zmagal si!", player, "skrha", computer)
    elif player == "Papir":
        if computer == "Škarje":
            print("Izgubil si!", computer, "režejo", player)
        else:
            print("Zmagal si!", player, "prekrije", computer)
    elif player == "Škarje":
        if computer == "Kamen":
            print("Izgubil si...", computer, "skrha", player)
        else:
            print("Zmagal si!", player, "režejo", computer)
    else:
        print("To ni veljavna izbira, preveri črkovanje!")
    player = False
    computer = t[randint(0,2)]