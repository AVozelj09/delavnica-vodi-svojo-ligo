# Vodi svojo ligo

[![Meet&Code](https://www.meet-and-code.org/img/logos/meet_and_code_logo_rgb.png)](https://www.meet-and-code.org/si/sl/)

[![Vodi svojo ligo](http://sportnodrustvo.polsnik.si/media/552188/glasujvodiligo_496x160.jpg)](https://www.meet-and-code.org/si/sl/event-show/1210)

V dveh triurnih delavnicah se otroci in mladostniki spoznajo z novimi možnostmi, ki jih ponuja svet računalnikov. Želja je, da bi spoznali nove načine "pametne" uporabe teh že tako "pametnih" naprav, ki pa jih po večini koristimo le za brskanje po internetu in igranje igric.
V svet osnov programiranja se odpravimo korakoma in zlagoma, zato predznanje ni potrebno. Seveda pa to tudi ni ovira, da bi se ne udeležili delavnic. Maksimalno število udeležencev na eni delavnici je 10. Udeleženci v dvojicah odkrivajo in rešujejo programske probleme na računalnikih Raspberry Pi 3 B+, ki jih priskrbi organizator. Na delavnicah se srečamo z:

  - Scratch (https://scratch.mit.edu/)
  - Python (https://www.python.org/)
  - SQLite (https://www.sqlite.org/)
  - HTML (https://www.w3schools.com/html/)
  - Raspberry Pi (https://www.raspberrypi.org/)

## Programiranje za vse

Ni nujno, da vaš otrok želi postati programer. Osnovna znanja programiranja pomagajo razvijati logično sklepanje, abstrakcijo, algoritmično reševanje, itd. Za punce in fante.

## Program delavnic

- Uvod (kaj je računalnik, o Raspberry Pi, itd.)
- Uvod v Scratch
- Maček nariše igrišče v Scratch-u
- Igra labirint zadeni gol v Scratch-u
- Primerjava Scratch : Python
- Python igra Kamen, škarje ali papir
- Uvod v HTML jezik
- Spremeni HTML kodo strani in zamenjaj junaka
- Uvod v delo s podatkovnimi bazami
- S tabelama rezultati in ekipe do lestvice
- "Fizično računalništvo" v Scratch-u (Physical Computing With Scratch)



### Todos

 - Python-HTML-SQLite application to manage simple league

# Licenca
----
MIT
Copyright 2018, Športno društvo Polšnik
**Free Software, Hell Yeah!**