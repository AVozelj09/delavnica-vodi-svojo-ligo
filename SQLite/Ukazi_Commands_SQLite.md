# Ukazi SQLite za delo s podatkovno bazo liga / SQLite commands for working with league database

## 1. Kreiranje tabele ekipe / Create table teams (ekipe)
```sqlite3
CREATE TABLE `ekipe` (
`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
`ime`	TEXT NOT NULL UNIQUE,
`logo`	TEXT
);
```

## 2. Kreiranje tabele rezultati / Create table results (rezultati)
```sqlite3
CREATE TABLE `rezultati` (
`idrezultata`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
`iddomacina`	INTEGER NOT NULL,
`idgosta`	INTEGER NOT NULL,
`goli_domacina`	INTEGER,
`goli_gosta`	INTEGER
);
```

## 3. Vnos ekip v bazo / Inserting teams into table
```sqlite3
INSERT INTO ekipe(ime) VALUES ('Polšnik');
INSERT INTO ekipe(ime) VALUES ('Sava');
INSERT INTO ekipe(ime) VALUES ('Čeznem da te zeznem');
INSERT INTO ekipe(ime) VALUES ('Janče');
INSERT INTO ekipe(ime) VALUES ('Zgornji Log');
INSERT INTO ekipe(ime) VALUES ('Hotič');
INSERT INTO ekipe(ime) VALUES ('Prežganje');
INSERT INTO ekipe(ime) VALUES ('Kandidati');
```

## 4. Izpis vseh vnosov v tabeli / List All teams
```sqlite3
SELECT * FROM ekipe;
```

## 5. Izbira WHERE / Using WHERE clause
```sqlite3
SELECT * FROM ekipe WHERE id>3;
```

## 6. Sortiranje / Sort
```sqlite3
SELECT ime FROM ekipe ORDER BY ime;
```

## 7. Izbris DELETE glede na izbiro WHERE / DELETE record using WHERE
```sqlite3
DELETE FROM ekipe where id=8;
SELECT * FROM ekipe;
```

## 8. Posodobitev podatka UPDATE glede na izbiro WHERE / UPDATE record using WHERE
```sqlite3
UPDATE ekipe SET ime='ŠD Polšnik' where id=1;
```

## 9. Vnos razporeda in rezultatov odigranih tekem / Inserting of schedule and some available results
```sqlite3
INSERT INTO rezultati(iddomacina,idgosta,goli_domacina,goli_gosta,datum) VALUES (7,4,4,2,1536343200);
INSERT INTO rezultati(iddomacina,idgosta,goli_domacina,goli_gosta,datum) VALUES (1,6,2,5,1536423300);
INSERT INTO rezultati(iddomacina,idgosta,goli_domacina,goli_gosta,datum) VALUES (3,2,1,4,1536429600);
INSERT INTO rezultati(iddomacina,idgosta,goli_domacina,goli_gosta,datum) VALUES (2,7,2,2,1536948000);
INSERT INTO rezultati(iddomacina,idgosta,goli_domacina,goli_gosta,datum) VALUES (4,5,1,5,1536948000);
INSERT INTO rezultati(iddomacina,idgosta,goli_domacina,goli_gosta,datum) VALUES (1,3,3,3,1537032600);
INSERT INTO rezultati(iddomacina,idgosta,goli_domacina,goli_gosta,datum) VALUES (7,1,5,2,1537551000);
INSERT INTO rezultati(iddomacina,idgosta,goli_domacina,goli_gosta,datum) VALUES (5,2,NULL,NULL,1546299000);
INSERT INTO rezultati(iddomacina,idgosta,goli_domacina,goli_gosta,datum) VALUES (3,6,NULL,NULL,1546214400);
INSERT INTO rezultati(iddomacina,idgosta,goli_domacina,goli_gosta,datum) VALUES (6,4,5,2,1538154000);
INSERT INTO rezultati(iddomacina,idgosta,goli_domacina,goli_gosta,datum) VALUES (1,5,1,13,1538240400);
INSERT INTO rezultati(iddomacina,idgosta,goli_domacina,goli_gosta,datum) VALUES (3,7,NULL,NULL,1546297200);
INSERT INTO rezultati(iddomacina,idgosta,goli_domacina,goli_gosta,datum) VALUES (7,6,4,2,1538758800);
INSERT INTO rezultati(iddomacina,idgosta,goli_domacina,goli_gosta,datum) VALUES (4,2,3,9,1538758800);
INSERT INTO rezultati(iddomacina,idgosta,goli_domacina,goli_gosta,datum) VALUES (5,3,4,1,1538839800);
INSERT INTO rezultati(iddomacina,idgosta,goli_domacina,goli_gosta,datum) VALUES (6,2,NULL,NULL,1539362700);
INSERT INTO rezultati(iddomacina,idgosta,goli_domacina,goli_gosta,datum) VALUES (7,5,NULL,NULL,1539443700);
INSERT INTO rezultati(iddomacina,idgosta,goli_domacina,goli_gosta,datum) VALUES (1,4,NULL,NULL,1539448200);
INSERT INTO rezultati(iddomacina,idgosta,goli_domacina,goli_gosta,datum) VALUES (2,1,NULL,NULL,1539966600);
INSERT INTO rezultati(iddomacina,idgosta,goli_domacina,goli_gosta,datum) VALUES (4,3,NULL,NULL,1539966600);
INSERT INTO rezultati(iddomacina,idgosta,goli_domacina,goli_gosta,datum) VALUES (5,6,NULL,NULL,1540053000);
```

## 10. Izpis rezultatov, razporeda / Showing of results table
```sqlite3
SELECT iddomacina, idgosta, goli_domacina, goli_gosta, datum
FROM rezultati;
```

## 11. Izpis razporeda z izpisom imen ekip / Schedule show with team names
```sqlite3
SELECT r.iddomacina, r.idgosta, d.ime as imedomacina, g.ime as imegosta, 
r.goli_domacina, r.goli_gosta, r.datum
FROM rezultati AS r
INNER JOIN ekipe AS d
ON r.iddomacina=d.id
INNER JOIN ekipe AS g
ON r.idgosta=g.id
```

## 12. Osvojene točke domačina in gol razlika / Home team points and goal difference
```sqlite3
SELECT idrezultata, iddomacina as idekipe, 
case when goli_domacina>goli_gosta then 
3 
else (
  case when goli_domacina<goli_gosta then 
  0 
  else 
  1 
  end
  )
end as tocke,
goli_domacina-goli_gosta as golrazlika 
FROM rezultati 
WHERE goli_domacina not null AND goli_gosta not null;
```

## 13. Osvojene točke gosta in gol razlika / Away team points and goal difference
```sqlite3
SELECT idrezultata, idgosta as idekipe, 
case when goli_domacina<goli_gosta then 
3 
else (
  case when goli_domacina>goli_gosta then 
  0 
  else 
  1 
  end
  )
end as tocke,
goli_gosta-goli_domacina as golrazlika 
FROM rezultati 
WHERE goli_domacina not null AND goli_gosta not null;
```

## 14. Združitev po ekipah GROPUP BY / Summation and Group By team
```sqlite3
SELECT s.idekipe, SUM(tocke) as tocke, SUM(golrazlika) as golrazlika 
FROM
(SELECT idrezultata, iddomacina as idekipe, 
case when goli_domacina>goli_gosta then 
3 
else (
  case when goli_domacina<goli_gosta then 
  0 
  else 
  1 
  end
  )
end as tocke,
goli_domacina-goli_gosta as golrazlika 
FROM rezultati 
WHERE goli_domacina not null AND goli_gosta not null
UNION ALL
SELECT idrezultata, idgosta as idekipe, 
case when goli_domacina<goli_gosta then 
3 
else (
  case when goli_domacina>goli_gosta then 
  0 
  else 
  1 
  end
  )
end as tocke,
goli_gosta-goli_domacina as golrazlika 
FROM rezultati 
WHERE (goli_domacina not null AND goli_gosta not null)) as s
GROUP BY s.idekipe
ORDER BY tocke DESC
```

## 15. Končna rešitev za izpis lestvice / Final Standings table
```sqlite3
SELECT s.idekipe,e.ime, SUM(tocke) as tocke, SUM(golrazlika) as golrazlika 
FROM
(SELECT idrezultata, iddomacina as idekipe, 
case when goli_domacina>goli_gosta then 
3 
else (
  case when goli_domacina<goli_gosta then 
  0 
  else 
  1 
  end
  )
end as tocke,
goli_domacina-goli_gosta as golrazlika 
FROM rezultati 
WHERE goli_domacina not null AND goli_gosta not null
UNION ALL
SELECT idrezultata, idgosta as idekipe, 
case when goli_domacina<goli_gosta then 
3 
else (
  case when goli_domacina>goli_gosta then 
  0 
  else 
  1 
  end
  )
end as tocke,
goli_gosta-goli_domacina as golrazlika 
FROM rezultati 
WHERE (goli_domacina not null AND goli_gosta not null)) as s
INNER JOIN ekipe AS e ON e.id=s.idekipe
GROUP BY s.idekipe
ORDER BY tocke DESC
```

# Naloga / Homework
Napiši sklic, ki bo izračunal povprečje zadetkov na tekmo. / Write a query which will calculate an average of goals per match.